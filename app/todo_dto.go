package app

import (
	"time"
)

type Tasks struct {
	ID        uint `gorm:"primary_key"`
	Title     string
	IsDone    bool
	CreatedAt time.Time
}

type TodoResponseDTO struct {
	Success bool
	Message string
	Data    interface{}
}

type TaskDTO struct {
	ID        uint
	Title     string
	IsDone    bool
	CreatedAt time.Time
}

func TodoInsertDto(todoDTO TaskDTO) Tasks {
	return Tasks{
		Title:     todoDTO.Title,
		IsDone:    false,
		CreatedAt: time.Now(),
	}
}

func ToTodoDTO(todo Tasks) TaskDTO {
	return TaskDTO{
		ID:        todo.ID,
		Title:     todo.Title,
		IsDone:    todo.IsDone,
		CreatedAt: todo.CreatedAt,
	}
}
