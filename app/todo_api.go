package app

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type TodoAPI struct {
	TodoRepository TodoRepository
}

func NewTodoAPI(p TodoRepository) TodoAPI {
	return TodoAPI{TodoRepository: p}
}

func (p *TodoAPI) FindAll(c *gin.Context) {
	var err error
	todos := []Tasks{}
	todos, err = p.TodoRepository.FindAll()

	if err != nil {
		response := TodoResponseDTO{false, err.Error(), ""}
		c.JSON(http.StatusNotFound, response)
		return
	}

	response := TodoResponseDTO{true, "", todos}
	c.JSON(http.StatusOK, response)

}

func (p *TodoAPI) FindByID(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		response := TodoResponseDTO{false, err.Error(), ""}
		c.JSON(http.StatusBadRequest, response)
		return
	}

	todo, err := p.TodoRepository.FindByID(uint(id))
	if err != nil {
		response := TodoResponseDTO{false, err.Error(), ""}
		c.JSON(http.StatusNotFound, response)
		return
	}

	response := TodoResponseDTO{true, "", ToTodoDTO(todo)}
	c.JSON(http.StatusOK, response)
}

func (p *TodoAPI) Create(c *gin.Context) {
	var todoDto TaskDTO
	err := c.BindJSON(&todoDto)
	if err != nil {
		response := TodoResponseDTO{false, err.Error(), ""}
		c.JSON(http.StatusBadRequest, response)
		return
	}

	if todoDto.Title == "" {
		response := TodoResponseDTO{false, "Title is empty", ""}
		c.JSON(http.StatusBadRequest, response)
		return
	}

	createTodo, err := p.TodoRepository.Save(TodoInsertDto(todoDto))
	if err != nil {
		response := TodoResponseDTO{false, err.Error(), ""}
		c.JSON(http.StatusBadRequest, response)
		return
	}

	response := TodoResponseDTO{true, "Todo added successfully!", ToTodoDTO(createTodo)}
	c.JSON(http.StatusOK, response)
}

func (p *TodoAPI) Update(c *gin.Context) {
	var todoDto TaskDTO
	err := c.BindJSON(&todoDto)
	if err != nil {
		response := TodoResponseDTO{false, err.Error(), ""}
		c.JSON(http.StatusBadRequest, response)
		return
	}

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		response := TodoResponseDTO{false, err.Error(), ""}
		c.JSON(http.StatusBadRequest, response)
		return
	}

	if todoDto.Title == "" {
		response := TodoResponseDTO{false, "Title is empty", ""}
		c.JSON(http.StatusBadRequest, response)
		return
	}

	todo, err := p.TodoRepository.FindByID(uint(id))
	if err != nil {
		response := TodoResponseDTO{false, err.Error(), ""}
		c.JSON(http.StatusNotFound, response)
		return
	}

	todo.Title = todoDto.Title
	updatedTodo, err := p.TodoRepository.Save(todo)
	if err != nil {
		response := TodoResponseDTO{false, err.Error(), ""}
		c.JSON(http.StatusNotFound, response)
		return
	}

	response := TodoResponseDTO{true, "", ToTodoDTO(updatedTodo)}
	c.JSON(http.StatusOK, response)
}

func (p *TodoAPI) Done(c *gin.Context) {
	var todoDto TaskDTO
	err := c.BindJSON(&todoDto)
	if err != nil {
		response := TodoResponseDTO{false, err.Error(), ""}
		c.JSON(http.StatusBadRequest, response)
		return
	}

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		response := TodoResponseDTO{false, err.Error(), ""}
		c.JSON(http.StatusBadRequest, response)
		return
	}

	todo, err := p.TodoRepository.FindByID(uint(id))
	if err != nil {
		response := TodoResponseDTO{false, err.Error(), ""}
		c.JSON(http.StatusNotFound, response)
		return
	}

	todo.IsDone = todoDto.IsDone
	updatedTodo, err := p.TodoRepository.Save(todo)
	if err != nil {
		response := TodoResponseDTO{false, err.Error(), ""}
		c.JSON(http.StatusNotFound, response)
		return
	}

	response := TodoResponseDTO{true, "", ToTodoDTO(updatedTodo)}
	c.JSON(http.StatusOK, response)
}

func (p *TodoAPI) Delete(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		response := TodoResponseDTO{false, err.Error(), ""}
		c.JSON(http.StatusBadRequest, response)
		return
	}

	todo, err := p.TodoRepository.FindByID(uint(id))
	if err != nil {
		response := TodoResponseDTO{false, err.Error(), ""}
		c.JSON(http.StatusNotFound, response)
		return
	}

	err = p.TodoRepository.Delete(todo.ID)
	if err != nil {
		response := TodoResponseDTO{false, err.Error(), ""}
		c.JSON(http.StatusNotFound, response)
		return
	}

	response := TodoResponseDTO{true, "Todo deleted successfully!", ""}
	c.JSON(http.StatusOK, response)
}

// Migrate ...
func (p *TodoAPI) Migrate() {
	p.TodoRepository.Migrate()
}
