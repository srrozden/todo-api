package app

import (
	"github.com/jinzhu/gorm"
	"log"
)

type TodoRepository struct {
	DB *gorm.DB
}

func NewTodoRepository(db *gorm.DB) TodoRepository {
	return TodoRepository{DB: db}
}

func (p *TodoRepository) FindAll() ([]Tasks, error) {
	todo := []Tasks{}
	err := p.DB.Find(&todo).Error
	return todo, err
}

func (p *TodoRepository) FindByID(id uint) (Tasks, error) {
	todo := Tasks{}
	err := p.DB.Where(`id = ?`, id).First(&todo).Error
	return todo, err
}

func (p *TodoRepository) Save(todo Tasks) (Tasks, error) {
	err := p.DB.Save(&todo).Error
	return todo, err
}

func (p *TodoRepository) Delete(id uint) error {
	err := p.DB.Delete(&Tasks{ID: id}).Error
	return err
}

func (p *TodoRepository) Migrate() {
	err := p.DB.AutoMigrate(&Tasks{}).Error
	if err != nil {
		log.Println(err)
	}
}
