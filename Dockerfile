FROM golang:1.13-alpine AS builder
WORKDIR /app
RUN apk add gcc g++ --no-cache
COPY go.mod .
COPY go.sum .
RUN go mod download

COPY main.go .
COPY ./app ./app
COPY ./pkg ./pkg

RUN CGO_ENABLED=1 GOOS=linux go build -a --ldflags="-s" -o todo-api

FROM alpine:3.11

COPY --from=builder /app/todo-api /app/todo-api

WORKDIR /app

RUN mkdir store

ENTRYPOINT ["/app/todo-api"]
