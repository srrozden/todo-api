package main

import (
	"encoding/json"
	"github.com/srrozden/todo-api/app"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/srrozden/todo-api/pkg/router"
	"github.com/stretchr/testify/assert"
)

func TestGetMethod(t *testing.T) {
	gin.SetMode(gin.TestMode)

	var resultModel app.TodoResponseDTO

	var table = []struct {
		name         string
		method       string
		url          string
		returnObject interface{}
	}{
		{"GET All TODO", "GET", "/todo/", resultModel},
	}

	var assert = assert.New(t)
	// Test loop for all table rows
	for _, row := range table {

		t.Run(row.name, func(t *testing.T) {
			req, w := makeGetRequest(row.method, row.url)

			assert.Equal(row.method, req.Method, "HTTP request method error")
			assert.Equal(http.StatusOK, w.Code, "HTTP request status code error")

			body, err := ioutil.ReadAll(w.Body)
			assert.Nil(err)

			switch row.name {
			case "GET All TODO":
				result := row.returnObject
				err = json.Unmarshal(body, &result)
				assert.Nil(err)
			}
		})
	}
}

func makeGetRequest(method, url string) (*http.Request, *httptest.ResponseRecorder) {
	r := router.Setup()
	w := httptest.NewRecorder()
	req, _ := http.NewRequest(method, url, nil)
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json")
	r.ServeHTTP(w, req)
	return req, w
}
