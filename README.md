# Todo Go API

## Development usage
Install Go to your computer. Pull the server repo. Execute the command in server folder.
```
go run main.go
```

## Test
```
go test
```
## Postman Collection
```
todo_api_postman.json
```

## Docker
```
docker-compose up --build
```

## Clients
[Todo Web](https://github.com/srrozden/todo-web)  