module github.com/srrozden/todo-api

go 1.14

require (
	github.com/gin-gonic/gin v1.6.2
	github.com/gorilla/mux v1.7.4 // indirect
	github.com/jinzhu/gorm v1.9.12
	github.com/mingrammer/go-todo-rest-api-example v0.0.0-20190527014715-ae46b4d42804 // indirect
	github.com/spf13/viper v1.6.2
	github.com/stretchr/testify v1.4.0
)
