package main

import (
	"github.com/srrozden/todo-api/pkg/config"
	"github.com/srrozden/todo-api/pkg/database"
	"github.com/srrozden/todo-api/pkg/router"
)

func init() {
	config.Setup()
	database.Setup()
}

func main() {
	config := config.GetConfig()

	r := router.Setup()
	r.Run("0.0.0.0:" + config.Server.Port)
}
