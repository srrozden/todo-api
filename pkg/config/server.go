package config

type ServerConfiguration struct {
	Port     string
	Username string
	Password string
	Timeout  int
}
