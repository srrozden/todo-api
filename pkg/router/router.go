package router

import (
	"github.com/jinzhu/gorm"
	"github.com/srrozden/todo-api/app"
	"github.com/srrozden/todo-api/pkg/database"
	"github.com/srrozden/todo-api/pkg/middleware"

	"github.com/gin-gonic/gin"
)

func Setup() *gin.Engine {
	r := gin.New()

	r.Use(gin.Logger())
	r.Use(gin.Recovery())
	r.Use(middleware.CORS())

	db := database.GetDB()
	todoAPI := InitTodoAPI(db)

	todo := r.Group("/todo")
	{
		todo.GET("/", todoAPI.FindAll)
		todo.GET("/:id", todoAPI.FindByID)
		todo.POST("/", todoAPI.Create)
		todo.PUT("/:id", todoAPI.Update)
		todo.PUT("/:id/done", todoAPI.Done)
		todo.DELETE("/:id", todoAPI.Delete)
	}

	// Protection for route/endpoint scaning
	r.NoRoute(func(c *gin.Context) {
		c.JSON(404, gin.H{"status": "Error", "message": "Page not found"})
	})

	return r
}

// InitTodoAPI ..
func InitTodoAPI(db *gorm.DB) app.TodoAPI {
	todoRepository := app.NewTodoRepository(db)
	todoAPI := app.NewTodoAPI(todoRepository)
	todoAPI.Migrate()
	return todoAPI
}
